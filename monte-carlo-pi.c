#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        printf("Wrong number of arguments. Provide # of threads and # of trials.\n");
        return -1;
    }

    struct drand48_data *randstate = malloc(sizeof(struct drand48_data));
    srand48_r(time(0), randstate);

    int nthreads = atoi(argv[1]);
    int trials = atoi(argv[2]) / nthreads;
    int i;
    int hits = 0;
    double x, y;

    for(i = 0; i < trials; i++)
    {
        drand48_r(randstate, &x);
        drand48_r(randstate, &y);
        if(x*x + y*y <= 1.0)
            hits++;
    }
    free(randstate);
    double pi = hits / (double)trials * 4.0;
    printf("Estimate of pi: %.10f\n", pi);

    return 0;
}



#include <stdio.h>
#include <assert.h>
#include <pthread.h>

int counter = 0;
pthread_mutex_t lock;
pthread_mutex_t thread_complete_lock;
pthread_cond_t thread_complete_conditional;
pthread_mutex_t active_lock;
int active_threads = 2;
#define THREAD_COUNT 2

typedef struct {
  int id;
} thread_arg;


void *mythread(void *arg) {
    printf("%d start, counter = %d\n", ((thread_arg *) arg)->id, counter);
    int i;
    for(i = 0; i < 10000; i++) {
        pthread_mutex_lock(&lock);
        counter = counter + 1;
        pthread_mutex_unlock(&lock);
    }
    printf("%d done\n", ((thread_arg *) arg)->id);
    printf("%d aquiring thread end lock\n", ((thread_arg *) arg)->id);
    pthread_mutex_lock(&thread_complete_lock);
    printf("ending thread %d w/ signal\n", ((thread_arg *) arg)->id);
    pthread_cond_signal(&thread_complete_conditional);
    pthread_mutex_unlock(&thread_complete_lock);
    printf("thread %d ended\n", ((thread_arg *) arg)->id);
    pthread_mutex_lock(&active_lock);
    active_threads--;
    pthread_mutex_unlock(&active_lock);
    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t p1, p2;
    assert(pthread_mutex_init(&lock, NULL) == 0);
    pthread_mutex_init(&thread_complete_lock, NULL);
    pthread_cond_init(&thread_complete_conditional, NULL);
    int rc;
    printf("main: begin\n");
    printf("starting counter: %d\n", counter);

    thread_arg targ[2];
    targ[0].id = 0;
    targ[1].id = 1;
    pthread_mutex_lock(&thread_complete_lock);
    rc = pthread_create(&p1, NULL, mythread, &targ[0]); assert(rc == 0);
    rc = pthread_create(&p2, NULL, mythread, &targ[1]); assert(rc == 0);
    while(active_threads > 0){
      pthread_cond_wait(&thread_complete_conditional, &thread_complete_lock);
    }

    printf("main: end\n");
    printf("ending counter: %d\n", counter);
    return 0;
}

#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

pthread_mutex_t lock;
sem_t turn_a;
sem_t turn_b;
sem_t turn_c;
int counter;

void *thread_a(void *arg)
{
    while(1)
    {
        sem_wait(&turn_a);
        pthread_mutex_lock(&lock);
        if(counter >= 2000)
        {
            pthread_mutex_unlock(&lock);
            break;
        }
        counter++;
        printf("A: counter value: %d\n", counter);
        pthread_mutex_unlock(&lock);
        sem_post(&turn_b);
    }
    printf("A: done\n");
    sem_post(&turn_b);
    return NULL;
}

void *thread_b(void *arg)
{
    int done = 0;
    while(!done)
    {
        sem_wait(&turn_b);
        pthread_mutex_lock(&lock);
        if(counter >= 2000){
          pthread_mutex_unlock(&lock);
          break;
        }
        counter++;
        printf("B: counter value: %d\n", counter);

        pthread_mutex_unlock(&lock);
        sem_post(&turn_c);
    }
    printf("B: done\n");
    sem_post(&turn_c);
    return NULL;
}
void *thread_c(void *arg)
{
    int done = 0;
    while(!done)
    {
        sem_wait(&turn_c);
        pthread_mutex_lock(&lock);
        if(counter >= 2000){
          pthread_mutex_unlock(&lock);
          break;
        }
        counter++;
        printf("C: counter value: %d\n", counter);

        pthread_mutex_unlock(&lock);
        sem_post(&turn_a);
    }
    printf("C: done\n");
    sem_post(&turn_a);
    return NULL;
}
int main()
{
    pthread_t p1, p2, p3;
    counter = 0;

    pthread_mutex_init(&lock, NULL);
    sem_init(&turn_a, 0, 1); // this decides that A goes first
    sem_init(&turn_b, 0, 0);
    sem_init(&turn_c, 0, 0);

    pthread_create(&p2, NULL, thread_a, NULL);
    pthread_create(&p1, NULL, thread_b, NULL);
    pthread_create(&p3, NULL, thread_c, NULL);

    pthread_join(p1, NULL);
    pthread_join(p2, NULL);
    pthread_join(p3, NULL);

    return 0;
}

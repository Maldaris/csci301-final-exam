
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char** argv){
  if(argc < 3){
    printf("incorrect number of arguments.\n");
    return 0;
  } else {
    struct stat ifstat;
    int ofd = open(argv[3], O_WRONLY | O_CREAT, 0600);
    int ifd = open(argv[2], O_RDONLY);
    if(!ofd || !ifd){
        printf("filenames are invalid.\n");
        return 0;
    }
    int res = fstat(ifd, &ifstat);
    if(res == -1) return -1;
    off_t seekOffset = (off_t)(ifstat.st_size / atof(argv[1]));
    printf("%d\n", seekOffset);
    char buf;
    while(lseek(ifd, seekOffset, SEEK_CUR) < ifstat.st_size){
      read(ifd, &buf, 1);
      write(ofd, &buf, 1);
    }
    write(ofd, "\n", 1);
    close(ofd);
    close(ifd);
    return 0;
  }
}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct {
  float ret;
  int trials;
} thread_args;

void* montecarlo(void* arg){
  struct drand48_data randstate;
  srand48_r(time(0), &randstate);
  int i;
  int hits = 0;
  double x, y;

  for(i = 0; i < ((thread_args*) arg)->trials; i++)
  {
      drand48_r(&randstate, &x);
      drand48_r(&randstate, &y);
      if(x*x + y*y <= 1.0)
          hits++;
  }
  double pi = hits / (double)((thread_args*) arg)->trials * 4.0;

  ((thread_args*) arg)->ret = pi;
}


int main(int argc, char *argv[])
{
  if(argc != 3)
  {
      printf("Wrong number of arguments. Provide # of threads and # of trials.\n");
      return -1;
  }
  int nthreads = atoi(argv[1]);
  int trials = atoi(argv[2]) / nthreads;

  int i;
  pthread_t threads[nthreads];
  thread_args args[nthreads];
  for(i = 0; i < nthreads; i++){
    args[i].trials = trials;
    args[i].ret = 0.0;
    pthread_create(&threads[i], NULL, &montecarlo, &args[i]);
  }
  float estimate = 0.0;
  for(i = 0; i < nthreads; i++){
    pthread_join(threads[i], NULL);
  }
  for(i = 0; i < nthreads; i++){
    estimate += args->ret;
  }
  estimate /= nthreads;

  printf("Estimate of pi: %.10f\n", estimate);
  return 0;
}
